import UIKit
import Lottie

class ViewController: UIViewController {
    @IBOutlet var animationView: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationView.loopMode = .loop
        animationView.play()
    }
    
    @IBAction func doStartScalingButtonAction(_ sender: UIButton) {
        let firstAnimator = UIViewPropertyAnimator(duration: 0.5,
                                                   curve: .easeIn) {
                                                    self.animationView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
        
        let secondAnimator = UIViewPropertyAnimator(duration: 0.5,
                                                    curve: .easeIn) {
                                                        self.animationView.transform = .identity
        }
        
        firstAnimator.addCompletion { _ in
            secondAnimator.startAnimation()
        }
        
        firstAnimator.startAnimation()
    }
}

